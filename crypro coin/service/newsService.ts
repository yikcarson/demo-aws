import CryptoNewsApi from 'crypto-news-api'
import * as jsonfile from 'jsonfile'
import * as path from 'path'


const Api = new CryptoNewsApi('98279de07bae873bab40fe414fb91350')


export class NewsService {
    constructor() {
        setInterval(() => {

            this.updateNews()
            console.log('[News allready update!]')

        }, 60000)
    }

    async updateNews() {
        const newsData = await Api.getTopNews('en')
        return await jsonfile.writeFile(path.join(__dirname,'../jsonFile/news/news.json'), newsData, { spaces: 2 })
    }

    async getnews(page: number = 1) {
        const news = await jsonfile.readFile(path.join(__dirname,'../jsonFile/news/news.json'))
        const finallNumber = page*3
        const firstNumber = finallNumber-3
        const outnews=news.slice(firstNumber,finallNumber)
        await jsonfile.writeFile(path.join(__dirname,'../jsonFile/news/outNews.json'),outnews,{spaces:2})
        return outnews
     }

     async getNewslength(){
        const news = await jsonfile.readFile(path.join(__dirname,'../jsonFile/news/news.json'))
        const newsLength={}
        newsLength['length']=news.length
        return newsLength
     }
}




// async function creatNews() {
//     const newsData = await Api.getTopNews('en')
//     await jsonfile.writeFile('../jsonFile/loacl.json',newsData,{spaces:2})
//     return
// }
// creatNews()


// async function updateNews() {
//     const upData = await Api.getTopNews('en')
//     await jsonfile.writeFile('../jsonFile/update.json',upData,{spaces:2})
//     return
// }
// updateNews()

// async function compareNews() {
//     const localDate = await jsonfile.readFile('../jsonFile/loacl.json')
//     const update = await jsonfile.readFile('../jsonFile/update.json')
//     const a = localDate.length
//     const newsD = []
//     for (let dataUp of update) {
//         let b = 0
//         for (let dataLo of localDate) {
//             if (dataLo._id !== dataUp._id) {
//                 b = b + 1
//             }
//         }
//         if (b == a) {
//             newsD.push(dataUp)
//         }
//     }
//     await jsonfile.writeFile('../jsonFile/dNews.json', newsD, { spaces: 2 })
// }
// compareNews()

