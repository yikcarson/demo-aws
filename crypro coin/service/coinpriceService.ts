import * as jsonfile from 'jsonfile'
import * as path from 'path'
import { User } from '../model';
// import { userService } from './userService';

const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();


const coinsTypecoins = ["zrx", "bat", "btc", "bch", "eos", "etc", "eth", "ltc", "rep", "xlm", "xrp", "zec"]

export class CoinService {
    public constructor() {
        setInterval(async () => {

            await this.getPrice()
            console.log('[PriceData Update Allready]')

        }, 60000)

    }
    async  getPrice() {
        const news = []
        let newDatas = await CoinGeckoClient.coins.markets();
        for (let coin of coinsTypecoins) {
            for (let data of newDatas.data) {
                if (data.symbol == coin) {
                    news.push(data)
                }
            }
        }
        let oldDates = await jsonfile.readFile(path.join(__dirname, '../jsonFile/price/priceData.json'))
        for (let newDate of news) {
            for (let oldDate of oldDates) {
                if (newDate.symbol == oldDate.symbol) {
                    if (newDate.current_price > oldDate.current_price) {
                        newDate["change"]='+'
                    } else if (newDate.current_price < oldDate.current_price) {
                        newDate["change"]='-'
                    } else {
                       newDate.change='='
                    }
                }
            }
        }
        // [CODE REVIEW] add io.emit() here to notify update is available
       return await jsonfile.writeFile(path.join(__dirname, '../jsonFile/price/priceData.json'),news,{spaces:2})
    }


    async selectionOutPrice(coin:User["coin"]) {
        const prices = await jsonfile.readFile(path.join(__dirname, '../jsonFile/price/priceData.json'))
        const output = []
        for (let selection of coin) {
            for (let price of prices) {
                if (selection == price.symbol) {
                    output.push({ name: selection, price: price.current_price, change:price.change })
                }
            }
        }
        await jsonfile.writeFile(path.join(__dirname, '../jsonFile/price/outprice.json'), output, { spaces: 2 })
        return output
    }


}

