import * as jsonfile from 'jsonfile';
import { User } from '../model';
import * as  path from 'path';




export class CreateUserService {
    async createuser(user: User) {
        const users: User[] = await jsonfile.readFile(path.join(__dirname, '../jsonFile/user.json'))
        const exist = users.find((account) => user.useremail == account.useremail)
        if (exist) {
            return {msg:`${exist} is exist`}; // [CODE REVIEW] throw error
        }
        const data = {
            id: users.length + 1,
            username: user.username,
            useremail: user.useremail,
            password: user.password,
            coin: []
        }
        users.push(data)
        return await jsonfile.writeFile(path.join(__dirname, '../jsonFile/user.json'), users, { spaces: 2 })
    }
}

