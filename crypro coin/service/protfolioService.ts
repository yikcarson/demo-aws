// import * as jsonFile from 'jsonfile'
import fetch from 'node-fetch'
// import * as  path from 'path'

export class ProtFolioService {
    private cache = {};
    constructor(){

    }
    async getdata(coin: string, day: number) {
        if(this.cache[`${coin}-${day}`]!=null){
            return this.cache[`${coin}-${day}`]
        }
        const res = await fetch(`https://api.coingecko.com/api/v3/coins/${coin}/market_chart?vs_currency=usd&days=${day}`)
        let result = await res.json()
        let data = { "day": day }
        let time = []
        let value = []
        for (let a of result.prices) {
            time.push(a[0])
            value.push(a[1])
        }
        data['time'] = time
        data['value'] = value
        this.cache[`${coin}-${day}`] = data;
        setTimeout(()=>{
            this.cache[`${coin}-${day}`]=null
            console.log('cache Delete')
        },300* 1000) 
        // await jsonFile.writeFile(path.join(__dirname,`../jsonFile/protfolio/${coin}.json`),data, { spaces: 2 })
        return  data
    }

}

let a=new ProtFolioService()
a.getdata('bitcoin',1)