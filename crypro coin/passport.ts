import * as passport from 'passport';
import { User } from './model';
import * as Local from 'passport-local';
import * as jsonfile from 'jsonfile'
import * as  path from 'path';
import * as jsonFile from 'jsonfile';

const LocalStrategy = Local.Strategy

passport.use(new LocalStrategy(
//  [CODE REVIEW] {
//     usernameField:'email'
// }, 
    async function (username, password, done) {

        const Users: User[] = await jsonfile.readFile(path.join(__dirname, './jsonFile/user.json'))
        const exist = Users.find((user) => user.username == username)
        if (!exist) {
            return done(null, false)
        }
        if ((exist.username == username) && (exist.password == password)) {
            console.log("login ok");
            return done(null, exist);
        }

    }
)
)

// passport.serializeUser(function(user, done) {
//     done(null, user)
//   });
  
//   passport.deserializeUser(function(user, done) {
//     done(null, user);
//   });
  


passport.serializeUser(function (user: User, done) {
    done(null, user.id);
});

passport.deserializeUser(async function (id, done) {
    const users: User[] = await jsonFile.readFile(path.join(__dirname, './jsonFile/user.json'))
    const user = users.find((user) => user.id == id)
    if(user){
        done(null, user);
    }
    else{
        done(new Error(`User with id ${id} not found!`))
    }
});