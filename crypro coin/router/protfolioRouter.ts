import * as express from 'express';
import { ProtFolioService } from '../service/protfolioService'
import { Request, Response } from 'express';

export class ProtFolioRouter {
    constructor(private protfolio: ProtFolioService) {

    }

    router() {
        const router = express.Router()
        router.get('/', this.getdata)
        return router
    }

    private getdata = async (req: Request, res: Response) => {
        try {
            const {coin,day}=req.query
            res.json(await this.protfolio.getdata(coin,day))
        } catch (error) {
            res.json(error)
        }
    }


}
