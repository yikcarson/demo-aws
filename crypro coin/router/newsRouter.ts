import * as express from 'express';
import { Request,Response } from 'express';
import { NewsService } from '../service/newsService'


export class NewsRouter{
    constructor(private newsservice :NewsService ){

    }

    router(){
        const router =express.Router()
        router.get('/',this.outputNews)
        router.get('/length',this.newsLength)
        return router
    }


    private outputNews =async (req:Request,res:Response)=>{
        try {
            const {page}=req.query
            console.log(page)
            res.json(await this.newsservice.getnews(page))
        } catch (error) {
            res.json(error)
        }
    }

    private newsLength =async (req:Request,res:Response)=>{
        try {
            res.json(await this.newsservice.getNewslength())
        } catch (error) {
            res.json(error)
        }
    }
}