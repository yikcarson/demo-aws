import *as express from 'express'
import { Request, Response } from 'express'
import { CoinService } from '../service/coinpriceService'
import * as jsonfile from 'jsonfile'
import * as path from 'path'
import { User } from '../model';

export class coinPriceRouter {
    constructor(private coinservice: CoinService) {

    }

    router() {
        const router = express.Router()
        router.post('/price', this.outPut)
        router.get('/77', this.test)
        return router
    }

    private outPut = async (req: Request, res: Response) => {
        try {
            const users: User[] = await jsonfile.readFile(path.join(__dirname, '../jsonFile/user.json'))
            const exist = users.find((user) => user.id == req.user.id)
            if (exist) {
                exist.coin = req.body
                for (let user of users) {
                    if (user.id == exist.id) {
                        user.coin = req.body
                    }
                }
                await jsonfile.writeFile(path.join(__dirname, '../jsonFile/user.json'),users,{spaces :2})
                res.json(await this.coinservice.selectionOutPrice(exist.coin))
            } else {
                res.status(401)
            }

        } catch (error) {
            res.json(error)
        }
    }
    private test = (req: Request, res: Response) => {
        console.log(req.user)
        res.json('ooo')
    }

}