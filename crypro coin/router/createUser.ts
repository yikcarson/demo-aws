import * as express from 'express';
import { Response, Request } from 'express';
import { CreateUserService } from '../service/creatUserService';



export class CreateUserRouter {
    constructor(private createuserservice: CreateUserService) {

    }

    router() {
        const router = express.Router()
        router.post('/',this.creatUser)
        return router
    }

    private creatUser = async (req: Request, res: Response) => {
        // [CODE REVIEW] try/catch the error to do different actions (normal => redirect, error => show error)
        this.createuserservice.createuser(req.body)
        
        res.status(200).redirect('/index.html')
    }
}