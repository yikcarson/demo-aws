async function getFirstPage() {
    const res = await fetch('/news?page=1', {
        method: "GET",
        headers: { "content-Type": 'application/json' } // [CODE REVIEW] Get doesnt have content type
    })
    const result = await res.json()
    document.querySelector('#news-contents').innerHTML = ``
    for (let data of result) {
        document.querySelector('#news-contents').innerHTML += `
            <div class="media">
                <img src=${data.thumbnail}
                    class="align-self-center mr-3" alt="...">
                <div class="media-body">
                    <h5 class="mt-0">${data.title}</h5>
                    <p>${data.description}</p>
                    <a href=${data.url}>Read more</a>
                </div>
            </div>
            `
    }
}

async function newPageClick() {
    let newspages = document.querySelectorAll('.page-link')
    for (let newspage of newspages) {
        newspage.addEventListener('click', async () => {
            let page = newspage.getAttribute("page")
            const res = await fetch('/news?page=' + page, {
                method: "GET",
                headers: { "content-Type": 'application/json' }
            })
            // [CODE REVIEW] check res.status is ok or not, if 4xx, show the error message. if 5xx, show "service temporarily unavailable"
            let result = await res.json()
            document.querySelector('#news-contents').innerHTML = ``
            for (let data of result) {

                document.querySelector('#news-contents').innerHTML += `
            <div class="media">
                <img src=${data.thumbnail}
                    class="align-self-center mr-3" alt="...">
                <div class="media-body">
                    <h5 class="mt-0">${data.title}</h5>
                    <p>${data.description}</p>
                    <a href=${data.url}>Read more</a>
                </div>
            </div>
            `
            }
        })
    }
}

async function getlength() {
    res = await fetch('/news/length', {
        method: 'GET',
        headers: { "content-Type": 'application/json' },
    })
    const result = await res.json()
    let page = 0
    if (result['length'] % 3 != 0) {
        page = Math.ceil(result['length'] / 3)
    } else {
        page = result['length'] / 3
    }
    console.log(`total:${result['length']}`)
    console.log(`page:${page}`)
    document.querySelector('#newsPage').innerHTML = ``
    for (let PageNumber = 0; PageNumber < page; PageNumber++) {
        document.querySelector('#newsPage').innerHTML += `
        <li class="page-item"><span class="page-link" page=${PageNumber + 1} >${PageNumber + 1}</span></li>
        `
    }
    newPageClick()
}
window.onload = async () => {
    getlength()
    getFirstPage()
}

