var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'price',
            data: [],
            borderWidth: 1.5,
            fill: false,
            borderColor: '#ffcc00',
            backgroundColor: '#ffcc00',
            pointRadius: 0.1,
        }]
    },
    options: {
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                },
                ticks: {
                    fontColor: '#fff'
                },
                gridLines: {
                    display: true ,
                    color: "rgba(32, 30, 30, 0.747)"
                  },
            }],
            yAxes: [{
                ticks: {
                    fontColor: '#fff'
                },
                gridLines: {
                    display: true,
                    color: "rgba(32, 30, 30, 0.747)"
                }
            }]
        },
        legend: {
            display: false,
        }
    }
});

async function getDays(coin) {
    const dayButton = document.querySelectorAll('#days')
    for (let button of dayButton) {
        button.addEventListener('click', async () => {
            const day = button.getAttribute("days")
            const res = await fetch(`/protfolio?coin=${coin}&day=${day}`, {
                method: "GET",
                headers: { "content-Type": 'application/json' }
            })
            const result = await res.json()
            console.log(result)
            myChart.data.labels = result.time
            myChart.data.datasets[0].data = result.value
            myChart.options.scales.xAxes[0].time.unit = 'day'
            myChart.update()
        })
    }
}

async function protfolioButtonClick() {
    const protfolioButtons = document.querySelectorAll('.protfoliCoin')
    for (let button of protfolioButtons) {
        button.addEventListener('click', async () => {
            let coin = button.getAttribute('coin')
            const res = await fetch(`/protfolio?coin=${coin}&day=1`, {
                method: "GET",
                headers: { "content-Type": 'application/json' }
            })
            const result = await res.json()
            console.log(result)

            myChart.data.labels = result.time
            myChart.data.datasets[0].data = result.value
            myChart.options.scales.xAxes[0].time.unit = ''
            myChart.update()
            document.querySelector("#protfolio-foot").innerHTML = `
            <button class='protfoliCoin' id="days" days='7'>7days</button>
            <button class='protfoliCoin' id="days" days='14'>14days</button>
            <button class='protfoliCoin' id="days" days='30'>30days</button>
            `
            getDays(coin)
        })
    }
}
window.onload = () => {
    protfolioButtonClick()
    // getDays("bitcoin")
}


// function a() {
//     myChart.data.labels = [1, 2, 3, 4, 5, 6,7,8,9]
//     myChart.data.datasets[0].data = [1, 2, 3, 4, 5, 6,7,8,9]
//     myChart.update()
// }