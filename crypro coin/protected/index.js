const socket = io.connect();


socket.on('updatePrice', (data) => {
    console.log(data)
    let number = 0
    document.querySelector('#sel-content-all').innerHTML = ``

    for (let coin of data) {
        number = number + 1
        if (coin.change == '+') {
            document.querySelector('#sel-content-all').innerHTML += `
            <div class="row" id="sel-content">
            <div class="col-2" id="number">
                <span>${number}</span>
            </div>
            <div class="col-4">
                <img src="../CryproIcons/${coin.name}.png">
                <span>${coin.name}</span>
            </div>
            <div class="col-2">
                <span>$${coin.price}</span>
            </div>
            <div class="col-2" >
              <div class='up'><span><i class="fas fa-long-arrow-alt-up"></i></span></div>
            </div>
        </div>
            `
        } else if (coin.change == '-') {
            document.querySelector('#sel-content-all').innerHTML += `
            <div class="row" id="sel-content">
            <div class="col-2" id="number">
                <span>${number}</span>
            </div>
            <div class="col-4">
                <img src="../CryproIcons/${coin.name}.png">
                <span>${coin.name}</span>
            </div>
            <div class="col-2">
                <span>$${coin.price}</span>
            </div>
            <div class="col-2" >
              <div class='down'><span><i class="fas fa-long-arrow-alt-down"></i></span></div>
            </div>
        </div>
            `
        } else {
            document.querySelector('#sel-content-all').innerHTML += `
            <div class="row" id="sel-content">
            <div class="col-2" id="number">
                <span>${number}</span>
            </div>
            <div class="col-4">
                <img src="../CryproIcons/${coin.name}.png">
                <span>${coin.name}</span>
            </div>
            <div class="col-2">
                <span>$${coin.price}</span>
            </div>
            <div class="col-2" >
              <div id="PriceChange"><span>--</span></div>
            </div>
        </div>
            `
        }
    }
})




document.querySelector('#selCoin-Form').addEventListener('submit', async function (event) {
    event.preventDefault()
    const dataUpload =[] ;
    let data1 = document.querySelectorAll('[name=coins]:checked')
    for (let data of data1) {
        dataUpload.push(data.value)
    }
    const res = await fetch('/crypro/price', {
        method: 'POST',
        headers: {
            "content-Type": 'application/json'
        },
        body: JSON.stringify(dataUpload)
    })
    const result = await res.json()
    console.log(result)
    let number = 0;
    document.querySelector('#sel-content-all').innerHTML = ``
    for (let coin of result) {
        number = number + 1
        document.querySelector('#sel-content-all').innerHTML += `
        <div class="row" id="sel-content">
        <div class="col-2" id="number">
            <span>${number}</span>
        </div>
        <div class="col-4">
            <img src="../CryproIcons/${coin.name}.png">
            <span>${coin.name}</span>
        </div>
        <div class="col-2">
            <span>$${coin.price}</span>
        </div>
        <div class="col-2" >
          <div id="PriceChange"><span>--</span></div>
        </div>
    </div>
        `
    }
})



// document.querySelectorAll('#pageNumber').addEventListener('click',()=>{
//     alert('ok')
// })
// document.querySelector('#page1').addEventListener('click', (event) => {
//     event.preventDefault()

// })






// })

// `
//     <div class="media">
//         <img src=${a.thumbnail}
//             class="align-self-center mr-3" alt="...">
//         <div class="media-body">
//             <h5 class="mt-0">${a.title}</h5>
//             <p>${a.description}</p>
//             <a href=${a.url}>Read more</a>
//         </div>
//     </div>
//     `

async function getlength(){
    res=await fetch('/news/length',{
        method:'GET',
        headers:{"content-Type": 'application/json'},
    })
    const result=await res.json()
    let page = 0
    if (result['length'] % 3 != 0) {
        page = Math.ceil(result['length'] / 3)
    } else {
        page = result['length'] / 3
    }
    console.log(`total:${result['length']}`)
    console.log(`page:${page}`)
    document.querySelector('#newsPage').innerHTML = ``
    for (let PageNumber = 0; PageNumber < page; PageNumber++) {
        document.querySelector('#newsPage').innerHTML += `
        <li class="page-item"><a class="page-link" id="aaa" href="#">${PageNumber + 1}</a></li>
        `
    }
}

