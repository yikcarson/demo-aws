import * as express from 'express';
export function isLoggedIn(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    if (req.user) {
        next();
    } else {
        res.redirect('/index.html');
    }
}