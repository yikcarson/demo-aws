import * as express from 'express'
import * as http from 'http';
import * as bodyParser from 'body-parser'
import { CoinService } from './service/coinpriceService'
import { coinPriceRouter } from './router/coinpriceRouter'
import * as socketIO from 'socket.io';
import { NewsService } from './service/newsService'
import { NewsRouter } from './router/newsRouter'
import { ProtFolioService } from './service/protfolioService';
import { ProtFolioRouter } from './router/protfolioRouter';
import * as session from 'express-session'
import * as passport from 'passport'
import { CreateUserService } from './service/creatUserService'
import { CreateUserRouter } from './router/createUser';
import { isLoggedIn } from './isLogin'
import { userService } from './service/userService'

//——————————————————————————————————————————————————————————————————————————————————————————————————————
const coinService = new CoinService()
const newsService = new NewsService()
const protfolioservice = new ProtFolioService()
const createuserservice = new CreateUserService()
const userservice = new userService()
//——————————————————————————————————————————————————————————————————————————————————————————————————————
const coinRouter = new coinPriceRouter(coinService)
const newsRouter = new NewsRouter(newsService)
const protfolioRouter = new ProtFolioRouter(protfolioservice)
const creatUserRouter = new CreateUserRouter(createuserservice)
//——————————————————————————————————————————————————————————————————————————————————————————————————————
const app = express()
const server = new http.Server(app);
const io = socketIO(server);
//——————————————————————————————————————————————————————————————————————————————————————————————————————
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/news/', newsRouter.router())
app.use('/protfolio', protfolioRouter.router())
app.use('/users/adduser', creatUserRouter.router())
//——————————————————————————————————————————————————————————————————————————————————————————————————————
const seesionMideleWare = session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
})

app.use(seesionMideleWare);

io.use((socket, next) => {
    seesionMideleWare(socket.request, socket.request.res, next)
})


app.use(passport.initialize());
app.use(passport.session());



import './passport'

app.post('/login', passport.authenticate('local', { failureRedirect: '/login' }),
    (req, res) => {
        res.redirect('./welcome.html');
    })

//——————————————————————————————————————————————————————————————————————————————————————————————————————
app.use('/crypro', coinRouter.router())
//——————————————————————————————————————————————————————————————————————————————————————————————————————
app.use(express.static('public'))
app.use(/* [CODE REVIEW] '/portal',*/isLoggedIn, express.static('protected'))
app.get('/logout', (req, res) => {
    req.logOut();
    res.redirect('/')
})


io.on('connect', async (socket) => {
    if (!socket.request.session.passport) {
        socket.disconnect()
    }
    const user = await userservice.getUserByiId((socket.request.session.passport.user))
    if (user) {
        setInterval(async() => {
            const update=await userservice.getUserByiId((socket.request.session.passport.user))
            if(update){
                socket.emit('updatePrice', await coinService.selectionOutPrice(update.coin))
            }
        }, 30000)
    }
})

//——————————————————————————————————————————————————————————————————————————————————————————————————————
const Port = 8080;
server.listen(Port, () => {
    console.log(`Server is start!${Port}`)
})